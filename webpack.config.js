const path = require("path");
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebPackPlugin = require("html-webpack-plugin");

const htmlPlugin = new HtmlWebPackPlugin({
  template: "./src/index.html",
  filename: "./index.html"
});

module.exports = {
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.(css|scss)$/,
        loader: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader!sass-loader'
        }),
      }
    ]
  },
  entry: {
    js: ['whatwg-fetch', './src/index.js']
  },
  plugins: [htmlPlugin, new ExtractTextPlugin('style-[hash].css')],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'app-[hash].js'
  }
};