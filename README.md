Hey! Here is my completed test assignment for Signal Health. Some notes below:

* To get started, run `npm install` to install all dependencies first. Then run one (or a combination) of the commands listed below.

* The way this is architected, there is a backend node server, which serves a skeleton `index.html` page to the browser, which is filled via JS packaged by Webpack.
* Take a look at package.json, and you'll see a few commands available.
  * `dev` will spin up the server, compile the assets, and watch for changes
  * `build` destroys any previous assets in the `dist` directory, then compiles the assets anew
  * `server` merely spins up the server
  * The idea is you'd run `build` followed by `server` (or combine them into one command). Separating these commands may be unnecessary, but I thought it might be a good idea to keep asset compilation independent.
* Looking at `server.js`, you can see that I hit your API there, rather than on the frontend. I was experiencing CORS issues (as expected), so I moved this call to the backend.
* Just a heads up, there were several keys in the JSON response that contained colons
  * For exmple, take a look at `ThreadChat.js`, and you'll see an instance of `message["dateTime:"]` – the key itself has a colon, which is odd.
* I noticed your version had the member's names, while the API returned IDs, so I made a rudimentary map of IDs -> Names (in `ThreadChat.js`). This would obviously be need to be made more robust, but it got the job done.

Let me know any questions!