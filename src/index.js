import React from "react";
import ReactDOM from "react-dom";

import "./sass/styles.scss";

import Index from "./modules/Index";

ReactDOM.render(<Index />, document.getElementById("app"));