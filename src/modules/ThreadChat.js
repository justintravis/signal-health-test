import React, { Component } from 'react';
import moment from 'moment';

const MEMBERS = {
  1234: "Aalap Majmudar",
  8843: "Andrew Taylor"
};

export default class ThreadChat extends Component {
  constructor(props) {
    super(props)
  }

  getMemberById(member_id) {
    return MEMBERS[member_id];
  }

  getFormattedDate(date) {
    return moment(date).from(moment());
  }

  groupMessagesByDay(messages) {
    const groups = messages.reduce((groups, message) => {
      const date = message["dateTime:"].split(' ')[0];
      if (!groups[date]) {
        groups[date] = [];
      }
      groups[date].push(message);
      return groups;
    }, {});

    return groups;
  }
  getWelcomeMessage() {
    return (
      <div className="welcome-message">
        <div className="welcome-message__container">
          <div className="welcome-message__title">
            <h3><span><i className="fa fa-comments" aria-hidden="true"></i> General</span></h3>
          </div>
          <div className="welcome-message__text">
            Welcome to the thread
            <span className="welcome-message__name">
              <i className="fa fa-comments" aria-hidden="true"></i> General
            </span> created by
            <span className="welcome-message__name">Aalap Majmudar</span>on May 5, 2018.
          </div>
        </div>
      </div>
    )
  }

  renderConversations() {
    const { chat } = this.props;
    const groupedMessages = this.groupMessagesByDay(chat);
    return Object.keys(groupedMessages).map(date => {
      return (
        <div key={date}>
          <div className="date-separator"><span className="date" dateTime={date}>{this.getFormattedDate(date)}</span></div>
          {(() => {
            return groupedMessages[date].map((message, index) => {
              return (
                <div key={index}>
                  <div className="message-container">
                    <div className="message">
                      <div className="message-content">
                        <div className="message-header">
                          <div className="user-profile-menu dropdown btn-group"><span className="name">{this.getMemberById(message["userID:"])}</span></div>
                          <span className="time">{message["dateTime:"]}</span>
                        </div>
                        <div className="message-content-wrapper"><div className="message-body">{message.body}</div></div>
                      </div>
                    </div>
                  </div>
                </div>
              )
            })
          })()}
        </div>
      )
    })
  }

  render() {
    return (
      <div className="chat-wrapper">
        <div id="main">
          <div className="flex-content">
            <div id="chat-container" className="loaded">
              <div id="chat">
                {this.getWelcomeMessage()}
                {this.renderConversations()}
              </div>
              <div className="flex-footer">
                <div className="message-form">
                  <div className="message-input-wrapper">
                    <div className="message-form-upload">
                      <a role="link" className="message-form-action-item">
                        <i className="fa fa-paperclip" aria-hidden="true"></i>
                      </a>
                    </div>
                    <textarea className="form-control message-text" placeholder="Message" maxLength="4000" required="" style={{"height": "43px"}}></textarea>
                  </div>
                  <div className="users-typing"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}