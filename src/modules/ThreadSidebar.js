import React, { Component } from 'react';

export default class ThreadSidebar extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div id="sidemenu">
        <div className="flex-header">
          <div className="team-user">
            <div className="team-name">Signal Health</div>
            <div className="user-name">
              <span className="user-name__status-icon"><i className="fa fa-circle" aria-hidden="true"></i></span>
              <span className="user-name__text">Will Slone</span>
            </div>
          </div>
        </div>
        <div className="flex-content">
          <div className="rooms-menu">
            <div className="scrollbar">
              <div>
                <div style={{"paddingBottom": "20px"}}>
                  <div className="rooms-menu__scroll-wrap">
                    <div className="menu-nav m-b-20">
                      <div className="heading">
                        <h5>Threads</h5>
                        <div className="heading__action">
                          <a role="button"><i className="fa fa-plus-square-o" aria-hidden="true"></i></a>
                        </div>
                      </div>
                      <div name="sidemenu_room_82053">
                        <div className="item">
                          <div className="item-icon"><i className="fa fa-comments" aria-hidden="true"></i></div>
                          <div className="item-text">General</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}