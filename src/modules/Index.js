import React, { Component } from "react";
import "whatwg-fetch";

import ThreadSidebar from "./ThreadSidebar";
import ThreadChat from "./ThreadChat";

export default class Index extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {
    fetch('/thread/1')
    .then(resp => resp.json())
    .then(json => this.setState({"thread": json[0]}));
  }

  render() {
    if (!this.state.thread) return null;
    const { thread } = this.state;

    return (
      <div className="appContainer">
        <ThreadSidebar />
        <ThreadChat chat={thread["chat:"]} />
      </div>
    )
  }
};