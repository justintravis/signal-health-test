const http = require('http');
const fs = require('fs');
const path = require('path');

http.createServer(function (request, response) {
  if (request.url === '/') {
    fs.readFile('dist/index.html', function(error, content) {
      response.writeHead(200);
      response.end(content, 'utf-8');
    })
  } else if (request.url === '/thread/1') {
    var req = http.request({
      hostname: '104.236.53.44',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      }
    }, function(res) {
      var body = '';

      res.on('data', (chunk) => {
        body += chunk;
      });

      res.on('end', function() {
        response.writeHead(200);
        response.end(body, 'utf-8');
      });
    });

    req.write(JSON.stringify([{ "request-id": 1, "action-name": "getChat","userID": "1234"}]));
    req.end();
  } else {
    fs.readFile(`dist${request.url}`, function(error, content) {
      response.writeHead(200);
      response.end(content, 'utf-8');
    });
  }
}).listen(3000);
console.log('Server running at http://127.0.0.1:3000/');